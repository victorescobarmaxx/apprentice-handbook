![logo](https://bitbucket.org/maxxpotential/apprentice-handbook/raw/6b7df4925ae7562395996b1d95b20a3c55e59017/images/Logo-Horizontal-Black-Text.png?token=901392f5f57145fc9da0b8edee1cc079843b376d "Logo")

# Our Handbook

Welcome to MAXX Potential! We are glad that you are here, and committed to building a safe learning environment where we hope that you will grow. Be sure to ask questions and try new things. Learn. Share and help others. That's all part of our culture. 

We are craftspeople with a genuine passion for our trade, which we are truly grateful to be able to share with you. The most important thing to know when you start is that when it comes to technology, the only constant is _change_. Far more important than learning any particular tool or programming language is learning how to continuously learn and teach yourself new skills. We understand that you are new to this industry, and trust us, you have a LOT to learn. We all do. During your time here we will do our best to keep you challenged, while also adding valuable experience to your resumé and exposing you to many different customers and types of work.

Life has many twists and turns, and none of us are perfect. As long as each day we try to be a little better than we were they day before, together we can achieve some amazing things. Thank you for allowing me to serve you in this capacity, and the entire team at MAXX is honored to be part of your personal journey.

Make the most of this opportunity!!

— Kim

![headshot](https://bitbucket.org/maxxpotential/apprentice-handbook/raw/6b7df4925ae7562395996b1d95b20a3c55e59017/images/mahan100x100.jpg?token=5a04812f8a9778097076d5285f5266a8e869bc07 "Kim")

Founder and CEO of MAXX Potential

***


## Introduction

* [Vision and Mission](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/visionAndMission.md?at=master)
* [MAXX Values](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/maxxValues.md?at=master)
* [Purpose of this Handbook](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/purposeOfThisHandbook.md?at=master)

## Life At MAXX
* [First Day](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/orientation.md?at=master)
* [When Should I Talk To Who?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/communication.md?at=master)
* [How Should I Act?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/conduct.md?at=master)
* [How Do I Request Time Off?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/timeoff.md?at=master)
* [Can I Work From Home?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/remote.md?at=master)
* [What Should I Wear?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/dress.md?at=master)
* [How Do I Talk To Customers?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/customerCommunication.md?at=master)
* [How Do I Level Up?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/leveling.md?at=master)
* [What Will The Interview Be Like?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/interview.md?at=master)
* [Where Should I Learn This Stuff?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/learning.md?at=master)
* [What If I Need Anything Else?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/openDoor.md?at=master)

## Benefits and Perks
* [Experience](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/benefitsAndPerks/experience.md?at=master)
* [Journeyman Track](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/benefitsAndPerks/journeyman.md?at=master)
* [Bonus.ly](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/benefitsAndPerks/bonusly.md?at=master)
* [Freelancing](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/benefitsAndPerks/freelancing.md?at=master)
* [Holidays](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/benefitsAndPerks/holidays.md?at=master)
* [Health Benefits](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/benefitsAndPerks/healthBenefits.md?at=master)
* [Level 4 and Beyond](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/benefitsAndPerks/future.md?at=master)

## Employment Policies (The Legal Stuff)

The following section uses a more serious tone but is not intended to scare you away from feeling comfortable. Like everything else we do, it is in the interest of learning that we have decided to use such typical policies. Part of gaining experience in the field is learning how to conduct yourself in the way that is expected in almost any job you'll find. So while we expect you to take these policies seriously, we hope that they will not negatively affect your time here at MAXX. We trust you to use your best judgement, and become the ultimate new hire for a tech company in the future - so this stuff should be pretty obvious!

* [Password Management Policy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/passwordPolicy.md?at=master)
* [Equal Opportunity Employment](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/equalityPolicy.md?at=master)
* [Non-Harassment/Non-Discrimination Policy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/nhndPolicy.md?at=master)
* [At-Will Employment](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/atWillPolicy.md?at=master)
* [Wage and Compensation](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/wagePolicy.md?at=master)
* [Drug and Alcohol Policy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/drugPolicy.md?at=master)
* [Weapons Policy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/weaponsPolicy.md?at=master)
* [Employee Privacy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/privacyPolicy.md?at=master)
* [Company Property](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/assetsPolicy.md?at=master)
* [Attendance Policy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/attendancePolicy.md?at=master)
* [Leave Policy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/leavePolicy.md?at=master)
* [Discipline Policy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/disciplinePolicy.md?at=master)
* [Termination Policy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/terminationPolicy.md?at=master)
* [Inclement Weather Policy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/employmentPolicies/inclementWeather.md?at=master)

# Acknowledgements

*We give our thanks to the team at [Clef](https://github.com/clef/handbook) for the inspiration behind this handbook, which is a learning tool in and of itself! If you would like to propose any changes or enhancements, you can always generate a pull request. :)*
