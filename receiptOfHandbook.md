Acknowledgement of Receipt and Acceptance for Employee Handbook             


I acknowledge that I have been given access to the Employee Handbook. I understand that I am responsible for reading the information contained in the Handbook.

I understand that the handbook is intended to provide me with a general overview of the company’s policies and procedures. I acknowledge that nothing in this handbook is to be interpreted as a contract, expressed or implied, or an inducement for employment, nor does it guarantee my employment for any period of time.

 I understand and accept that my employment with MAXX Potential is at-will. I have the right to resign at any time with or without cause, just as the company may terminate my employment at any time with or without cause or notice, subject to applicable laws. I understand that nothing in the handbook or in any oral or written statement alters the at-will relationship, except by written agreement signed by the employee and CEO. 

I acknowledge that the company may revise, suspend, revoke, terminate, change or remove, prospectively or retroactively, any of the policies or procedures outlined in this handbook or elsewhere, in whole or in part, with or without notice at any time, at MAXX Potential’s sole discretion. 



_________________________________ 
(Signature of Employee) 

__________ 
(Date) 

_________________________________ 
(Company Representative)
