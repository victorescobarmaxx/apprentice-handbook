# Holidays

MAXX Potential is closed on the following days:

* New Year's Day   
* Memorial Day     
* Independence Day 
* Labor Day        
* Thanksgiving Thursday & Friday 
* Christmas Day    

These are unpaid holidays for Apprentices, who are not expected (or permitted) to report for work. Employees can arrange to make up the hours missed at another time during the same week. Please discuss with your advisor if you wish to come in outside of your normal schedule so they can prepare projects for you to work on. 

## Special Cases

### Time Sensitive Projects

If the project you are working on is time sensitive, you might be asked to work through a holiday, in which case holiday pay will be given during these shifts at a rate of one and one-half times your usual pay rate for the hours worked during the Holiday. Your advisor will discuss these situations with you on an as-needed basis. 

### 24/7 Coverage Projects

If you are assigned to a team with 24/7 coverage for the client, you may be required to work your scheduled shift on a holiday, unless arrangements are made ahead of time for coverage with your team leader. Holiday pay will be given during these shifts at a rate of one and one-half times your usual pay rate for the hours worked during the holiday. The holiday is considered to be the 24-hour period of the Holiday listed above. 

### Client-Observed Holidays

If a client you are working with is observing a holiday not listed above and MAXX Potential's services are not required that day, then you may have the day off as well. These will be unpaid days off, unless you arrange to make up the time with your advisor in advance. You may also work your normally scheduled shift, provided there is alternate work for you to perform on these days.

### Other Holidays

There are many other government, religious, and school holidays during which the MAXX Potential offices are open. If you would like to take the day off in observance of any other holiday, please make the request in advance through our time system. These absenses will be approved, but not paid.