# Journeyperson Certification

MAXX Potential is approved by the VA Department of Labor as a registered apprenticeship program. If you desire a certification to go along with your experience here then you'll be happy to know that by working for MAXX Potential and completing the required coursework, you may earn a state-issued Journeyman credential. Contact your advisor for more information.