# Wage and Compensation

## Pay Practices

Fair compensation is critical to making MAXX a desirable place to work and helping our employees lead happy, healthy lives inside and outside of the office. Our pay structure is designed to meet several goals. We seek to:

* Provide pay that reflects the value your work provides to the company.
* Ensure that there is transparency within the company, and fairness across the company.
* Make decisions about pay based on skills and value without regard to race, color, religion, age, national origin, marital status, pregnancy, disability, sexual orientation, gender, sex, gender identity or expression, genetic information, military status, or any other basis protected by applicable law.

### Payment Structure

The company will set the starting Level 1 pay above the “Living Wage for 1 Adult with 0 Children” as defined by “The Living Wage Calculator”. For more information, please visit the “Living Wage Calculator” website at [https://livingwage.mit.edu/](https://livingwage.mit.edu/).  The living wage will be assessed on an annual basis beginning in Q1 and will be applied by the end of Q2 for each MAXX Potential office. The pay structure is in-part based on an Apprentice’s level and is as follows:

Level| Hourly Rate
-----|---------------------------
  1  |   105% of the Living Wage
  2  |   105% of the Living Wage
  3  |   125% of the Living Wage
  4  |   130% of the Living Wage

### Payday Procedures

The MAXX work-week runs from Sunday to Saturday. Each pay period consists of two work-weeks and is paid out one-week after the pay period ends. This means that each paycheck you receive reflects the hours worked for the two-week period ending the previous Saturday. Payday is every other Friday for all employees. 

**You are responsible** for making sure your pay is **correct.** It is your responsibility to make sure that your hours are correct by the Monday before each payday. Please review your tax withholding and pay stub each pay period, particularly, the rate of pay, hours worked and deductions, to ensure that they are true and accurate. If your check is incorrect, please notify the Payroll Administrator (hr@maxxpotential.com) immediately.

### Work Schedules

Employees should set up their own work schedules through the company's time and attendance system. It is imperative that employees show up when they say they are available, so that the Leadership Team may plan projects and work around who will be in the building. Any major changes in availability should be discussed with your advisor with at least two weeks notice.

### Time Clock

Employees are to clock in and out from the time clock terminal. If the terminal is down due to connectivity issues, please contact the Payroll Administrator with an email of your in/out time immediately. Any abuse of the time clock system is subject to further disciplinary action, up to and including termination.


### Overtime Pay

Overtime is considered any amount of hours over 40 in a single workweek. You must have explicit permission from your advisor to work over 40 hours in a week, and will only be granted permission if there is a business justification for you to do so. If approved to work overtime, you will be paid one and one-half times your usual pay rate for all hours worked over 40 in a workweek.

### Night Shift Differential

For Apprentices that are assigned shifts that fall outside of the hours of 6 am-6 pm Monday-Friday, a shift differential will be paid for each of the hours worked for that shift. As of October 2019, the shift differential is an additional $1/hr. The differential does not apply to Apprentices who opt to work overtime on a case-by-case basis.
