# Drug and Alcohol Policy

Apprentice employees are prohibited from unlawfully consuming, distributing, possessing, selling, or using controlled substances while on duty. In addition, apprentices may not be under the influence of any controlled substance, such as drugs or alcohol, while at work, on company premises or engaged in company business. Anyone violating this policy may be subject to disciplinary action, up to and including termination.

Prescription drugs or over-the-counter medications, taken as prescribed, are permitted.

Certain customers may require that employees assigned to their projects be able to pass a drug screen. These screenings will be performed on a case-by-case basis, and require employee consent.