# Password Management Policy

## Purpose
LastPass is a password management platform designed to allow users to generate, store, and share strong complex passwords easily and securely. MAXX Potential’s implementation of LastPass will improve organizational security by increasing average strength of user passwords, eliminating password reuse, and leveraging the platform for access control to shared credentials.

## Scope
The LastPass policy outlined within this document applies to all employees of MAXX Potential, the work performed by them for the company, and the devices that work is performed on.

## Password Policy
All passwords used in the context of employment at MAXX Potential must be rated “Strong” by LastPass. We recommend a starting length of 14 characters for passwords. They should be as complex as you can make them and rotated every few months.

## Shared Credentials
Shared accounts and credentials are generally not permitted. Exceptions must be considered and approved by LastPass Administrators.

## Autofill
You should disable browser autofill for forms and login fields such that logging into a service requires opening your LastPass vault.

## Access Control
Access to shared credentials is controlled in LastPass on a need-to-know basis, with LastPass Admins revoking visibility when any employee leaves a given project or the company. Employees must not share credentials or access to an account or asset without the project manager’s prior approval.

## Multi-factor Authentication (MFA)
All LastPass Admins or users with access to admin console features must enable multi-factor authentication feature. All other users are encouraged, but not required to do so.

## For help with installing LastPass please refer to the [LastPass Installation Guide](https://docs.google.com/a/maxxpotential.com/document/d/19VeK25sMAdfh_hdoJWp20L5AvBTdG0rYm3N9UOi5NO4/edit?usp=sharing)
