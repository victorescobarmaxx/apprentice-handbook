# Inclement Weather Policy 

## Purpose
During inclement weather and other emergency situations at MAXX Potential, the safety of our employees is paramount. At the same time, MAXX provides important services to our clients and must maintain continuous effective business operations. With due consideration to safety, MAXX will remain open and operate normally to the greatest extent possible. All employees should evaluate their own circumstances carefully, exercise appropriate judgement, and take responsibility for their safety when making decisions during inclement weather.

In the instances of extreme weather conditions posing any serious danger, MAXX Potential will consider closing our offices and take steps to notify employees immediately.

This policy applies to all employees of MAXX Potential and all locations including physically present and employees working on-site with customers. 

## Business Closure
If inclement weather is predicted to begin before the start of our regular working hours, we will determine if it is in everyone’s best interests if the company closes its offices until the severe conditions have passed. If the inclement weather is predicted to occur during or after work hours, we may decide to let employees go home early, or find some other arrangement (e.g. work from home that day). If the city our operations are located in has declared a weather emergency, MAXX Potential will follow suit and close the workplace. 
***Employees will not be compensated if they are unable to make it into work or during business closure, unless they have received written approval from the Leadership team to work from home. Without approval to work from home, employees are NOT allowed to conduct in any business related activities, other than notifying their manager.

## Notification Procedure
When changes in hours of operations are necessary due to emergency situations such as inclement weather or loss of utilities, apprentices will be notified by their Allocation manager or a member of the leadership team immediately. Unless the office is closed, employees are expected to come to work.

## Notification of Employee lateness or absence due to inclement weather
Employees are requested to use good judgment in monitoring weather and travel conditions. We do not encourage employees to attempt driving when their safety would be endangered. This includes travel to and from work, as well as during the workday. If our business is open and you are going to be late, or you are unable to make it to work due to inclement weather, you must notify your Allocation Principal Technologist as soon as possible by phone, email or Slack message.  

##On-site Customer Employees:
If you are unable to attend work due to your inability to travel safely or other reason and the business remains open you must notify your Allocation Principal Technologist, along with your on-site customer contact.

##Management of Work Tasks During Inclement Weather
If you are unable to attend work due to your inability to travel safely or other reason and the business remains open, if your role includes direct and pre-arranged contact with customers and clients you should work with your Allocation Principal Technologist to contact any customers/clients who will be directly impacted by your absence (for example, if you had a scheduled meeting with a customer/client). 

