# Termination Policy

## Definitions

* Employment At Will refers to the Apprentices’s right to terminate his or her employment relationship with MAXX at any time and for any reason (or for no reason at all), and the right of MAXX to terminate the employment of any Apprentice at any time for any lawful reason, or for no reason at all.

* Involuntary Termination is the termination of the employment relationship between MAXX and an Apprentice resulting from the decision of MAXX.

* Job Abandonment refers to the Voluntary Termination of the employment relationship when an Apprentice who is not on authorized leave fails to show up to work or fails to contact his or her supervisor regarding the absence during the duration of the shift, unless there were extenuating circumstances and then it must be discussed as soon as feasibly possible.

* Layoff refers to Involuntary Termination that typically (although not exclusively) occurs due to lack of work, lack of funds, reorganization, elimination of position, reduction in force or grant expiration.

* Termination is the cessation of the employment relationship between MAXX and the Apprentice, regardless of the reason.

* Voluntary Termination is the termination of the employment relationship resulting from the voluntary decision of the Apprentice to resign from the employment of MAXX, and includes but is not limited to resignation, inability or failure to return from an authorized leave of absence, Job Abandonment and retirement.


## Procedures

Any termination of the employment relationship, whether voluntary or involuntary, must be treated in a confidential, professional manner by all concerned.  The Department of Human Resources will share this information with others at MAXX Potential as deemed necessary to complete the termination process and to resolve any issues relating to the termination.

1. Voluntary Termination

    An Apprentice who voluntarily terminates the employment relationship is expected to provide his or her supervisor with adequate written notice, and preferably no less than two weeks in advance, regardless of job classification.
    
    An Apprentice who voluntarily terminates the employment relationship is expected to work the entire notice period unless approved by the immediate supervisor.

2. Involuntary Termination

    Although Involuntary Terminations generally include prior notice to the affected Apprentice, MAXX reserves the right to bypass any notice as it deems necessary in light of the specific circumstances.

    In the event of gross misconduct by an Apprentice, Involuntary Termination may occur without prior notice. Gross misconduct may include, but is not limited to, the following:

    * Violation of any law, policy, regulation, or practice, including those related to the [Discipline Policy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/Employment%20Policies/Discipline%20Policy.md?at=master); 

    * Unlawful or improper conduct while on the time clock;

    * Conviction of a crime which may call into question the ability of an Apprentice to properly carry out the responsibilities of his/her/their position;

    * Violence or the threat of violence in the workplace;

    * Theft or fraud;

    * Lying or intentionally withholding information that should be disclosed;

    * Insubordination or refusal to perform work;

    * Serious negligence, recklessness, or intentional wrongdoing;

    * Acts of discrimination or retaliation; and/or

    * Demonstrated lack of respect for others.

The list set forth above is not exhaustive. Gross misconduct may require the supervisor to take adverse employment action, including but not limited to, any action outlined in the [Discipline Policy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/Employment%20Policies/Discipline%20Policy.md?at=master).

    A supervisor contemplating involuntary termination of an Apprentice must review the matter with the next level of management and their HR Partner prior to making a final determination.

    The supervisor is responsible for compiling complete and accurate documentation regarding any Involuntary Termination, regardless of the basis. 

    The HR Partner is responsible for conducting the termination meeting with the affected Apprentice, and for fully documenting the discussion.

    MAXX Potential will not re-employ anyone who has been involuntarily terminated from employment at MAXX for gross misconduct.

3. Separation Processing

    The immediate supervisor is responsible for completing the Separation Checklist, prior to the departure of the terminating Apprentice, in order to ensure the collection of all MAXX property, devices, etc., and the completion of other measures necessary to protect MAXX Potential during the separation

    The immediate supervisor is responsible for promptly contacting the Department of Human Resources with the letter of resignation or disciplinary form and all associated documents during the pay period in which the termination occurs.
    
    MAXX Potential may request an exit interview upon notice of termination. The purpose of the exit interview is to provide an opportunity for the terminating Apprentice to provide feedback to MAXX about the employment experience.

    All rights and privileges of employment with the company terminate upon the date of separation. Terminating employees are required to return all company property assigned to them within 24 hours of separation. Failure to do so may result in the withholding or reduction of their final paycheck.

    If a terminating Apprentice has left any personal property on MAXX Potential property, it will be the responsibility of the terminating Apprentice to pick it up within 2 weeks of separation from the company, during their regularly scheduled shifts. If he/she/they do not obtain their personal property within 2 weeks of separation, they may request MAXX package and send out the item(s) to the most recent current address on file. MAXX cannot be held liable for any personal property left at MAXX Potential, or due to the shipping process. If a terminating Apprentice wishes to pick up his/her/their property outside of these listed opportunities, they may arrange a time with their supervisor to come by, provided it is within the supervisors normal working hours.

## Final Paycheck

Employees who terminate employment with the company will be given their final paycheck in accordance with the next scheduled payday. Should the employee be unable to personally retrieve their paycheck, it will be mailed to the address on file or processed through direct deposit.

## At-Will Employment Not Affected

Notwithstanding anything to the contrary stated in this policy, nothing herein is intended to alter the at-will status of any Apprentice. MAXX at all times retains the right to terminate any Apprentice at any time for any lawful reason, or for no reason at all.


