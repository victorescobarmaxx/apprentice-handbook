## Weapons-Free Workplace

MAXX Potential maintains a work environment safe for all persons, including the community, and conducive to attaining high work standards. To achieve these objectives, MAXX is committed to a strong stand against firearms and weapons in the work environment. 

In order to maintain the firearms and weapons free work place, MAXX prohibits the possession of firearms and weapons regardless of any license or permit that an individual may have which would otherwise authorize the individual to carry firearms or weapons. This policy will be strictly enforced. 

Violation of this policy includes having weapons or firearms on one's person within the MAXX Office Building, as well as onsite with any MAXX Potential Client. Being found in violation of this policy will result in disciplinary action, as per the Discipline Policy.

##Definitions

1. Firearm: A weapon, a pistol or rifle, whether loaded or unloaded, capable of firing a projectile and using an explosive as a propellant.
2. Weapons: An instrument of attack or defense.