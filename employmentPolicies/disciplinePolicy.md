# Discipline Policy

## Grounds for Disciplinary Action

MAXX Potential reserves the right to discipline and/or terminate any employee who violates company policies, practices or rules of conduct. Poor performance and misconduct are also grounds for discipline or termination.

This following list exhibits the types of actions or events that are subject to disciplinary action. It is not intended to indicate every act that could lead to disciplinary action. MAXX Potential reserves the right to determine the severity and extent of any disciplinary action based on the circumstances of each case. These actions include, but are not limited to: 


* Engaging in acts of discrimination or harassment in the workplace; 
* Possessing, distributing or being under the influence of illicit controlled substances; 
* Being under the influence of a controlled substance or alcohol at work, on company premises,       
or while engaged in company business; 
* Unauthorized use of company property, equipment, devices or assets; 
* Damage, destruction or theft of company property, equipment, devices or assets; 
* Removing company property without prior authorization or disseminating company information without authorization; 
* Falsification, misrepresentation or omission of information, documents or records; 
* Lying; 
* Insubordination or refusal to comply with directives; 
* Failing to adequately perform job responsibilities; 
* Excessive or unexcused absenteeism or tardiness; 
* Disclosing confidential or proprietary company information without permission; 
* Illegal or violent activity; 
* Falsifying injury reports or reasons for leave; 
* Falsifying Clock in and out time in log;
* Possessing unauthorized weapons on premises; 
* Disregard for safety and security procedures; 
* No call/no show;
* Disparaging or disrespecting supervisors and/or co-workers; and 
* Any other action or conduct that is inconsistent with company policies, procedures, standards or expectations. 


## Procedures

Disciplinary action is any one of a number of options used to correct unacceptable behavior or actions. Discipline may take the form of oral warnings, written warnings, probation, suspension, demotion, discharge, removal or some other disciplinary action, in no particular order. The course of action will be determined by the company at its sole discretion as it deems appropriate.

## Termination

Employment with MAXX Potential is on an at-will basis and may be terminated voluntarily or involuntarily at any time. 

Upon termination, an employee is required to: 
* continue to work until the last scheduled day of employment; 
* turn in all reports and paperwork required to be completed by the employee when due  and no later than the last day of work; 
* return all files, documents, equipment, keys, access cards, software or other property belonging to the company that are in the employee’s possession, custody or control, and turn in all passwords to his/her advisor or Master Technologists; 
* participate in an exit interview as requested by the Vice President, Human Resources

See the [Termination Policy](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/Employment%20Policies/Termination%20Policies.md?at=master) for further details.
