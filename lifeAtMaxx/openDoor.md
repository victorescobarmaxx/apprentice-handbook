## What if I Need Anything Else?

MAXX Potential has an open door policy and takes apprentice employee concerns and problems seriously. MAXX Potential values each apprentice employee and strives to provide a positive work experience. You are encouraged to bring any workplace concerns or problems you might have or know about to your advisor or some other member of the management team. If you would like to submit any comments annoymously, feel free to do so through the [feedback form](http://feedback.maxxpotential.com), and MAXX will do everything they can to address these concerns as well. 
