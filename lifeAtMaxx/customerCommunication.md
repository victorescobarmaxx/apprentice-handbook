# How Do I Talk to Customers?

## Level 1

At Level 1, you might have to interact with customers with occasional emails. Any email sent to a client should be proof read by your advisor before it is sent. Emails should be written with proper business email ettiquette. You are representing MAXX Potential, and as such should take care when crafting any message to our clients. If your advisor allows it, you may advance to be able to contact clients without having your emails proofread, but you should CC your advisor and anyone else on the Leadership Team that might be involved with the project you are working on.

## Level 2

If you have made it to Level 2 and your advisor has approved of your ability to email clients, you will no longer need to have them proof read before making contact. However with any emails going out, you should CC your advisor as well as any one else working on the project including teammates and other Leadership Team members.

In addition to email communication, you might begin to be invited to meetings with the clients. During these meetings you should come prepared in proper dress and ready to take notes. As always, we want to put our best foot forward when representing MAXX Potential.

## Level 3 & 4

By these levels, you will most likely be involved with project management. It will be necessary to keep good communication open with the client of the project you are on, as well as with the Leadership Team about the development stages of your project.

At this stage, you might be required to go off-site to meet with your clients. Appropriate attire is a must, and as always you should take great pride in representing MAXX Potential outside of the office. 