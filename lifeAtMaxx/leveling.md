# How Do I Level Up?

These documents are meant to provide a guide to the skills necessary before advancing to each level. If you have free time to focus on learning, you can use these to figure out what you should be working on. If you think you meet all of the requirements on these guides and are ready to move to the next level, bring it up with your advisor during your next one on one. This is a valuable time where they can quiz you and see if they feel comfortable putting you up for the interview process. 

If chosen to interview, you can sign up for a time slot during the next interview day. See the [What Will The Interview Be Like?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/interview.md?at=master) Page for more information on how the interview process will go. 


* [Level 1 to 2](https://docs.google.com/a/maxxpotential.com/document/d/1EBhA2-2NeU96iJ9k26UtETRe4FNAVSCpFVTlHkUchRk/edit?usp=sharing)
* [Level 2 to 3](https://docs.google.com/a/maxxpotential.com/document/d/1gXkVT36hmjqiVNklT5PSyI-1yhXFgaAwR1dfav-xgAc/edit?usp=sharing)
* [Level 3 to 4](https://docs.google.com/a/maxxpotential.com/document/d/1su-5mLRH-Ubh0sZiTS4cuKpcAxA90pBF8lrnId57PjE/edit?usp=sharing)

*Note: The Titles are based on the level you are currently on, not the level you are aiming for in a promotion*