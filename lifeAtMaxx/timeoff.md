# How Do I Request Time Off?


If you would like to request time off, please fill out and submit the [Apprentice Time Off Request Form](http://rto.maxxpotential.com). Try to submit these requests at least 2 weeks in advance so that we cover any project requirements. Talk to your Advisor or the leadership team if you have any questions about whether the request will be approved.

It would also be a good idea to make sure that your Advisor and anyone on the team you are working on knows of your absence before you leave so they can be prepared to manage the workload without you.