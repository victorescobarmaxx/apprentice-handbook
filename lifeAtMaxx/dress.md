# What Should I Wear?

## Everyday Attendance

Here at MAXX we have a casual dress code. We trust you to use your best judgement and dress comfortably and appropriately. Please keep in mind that we often have clients and partners tour our space and we would like to put our best foot forward. These could be your future employers!

## Client Interaction

While representing MAXX Potential in direct interactions with our customers, we ask that you follow a business casual dress code. We always want to be perceived as prepared and professional.