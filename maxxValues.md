# MAXX Values

## Initiative
_We are all responsible for making things better._

If you see something that can be improved, propose a solution. Even better, implement one. We are never idle and always looking for ways to improve ourselves and the services we offer to our customers. Smart risk-taking in search of better solutions is encouraged and rewarded. 

## Curiosity
_Learning begins with "Why?" and "How?"_

Our culture is built on a foundation of continuous learning. We should always be asking questions and seeking answers. Try. Learn. Try again. See someone doing something interesting? Ask them how they did it. Hear a term you don't know? Ask what it means.

## Perseverance
_See things through._

It is only by pushing through the tough challenges that we develop our skills and expertise. We work hard and don't give up in the face of adversity, which is reflected in the quality of our products and services. If this stuff were easy, everyone would do it. It's not, and we know that. We also know that you have what it takes! 

## Authenticity
_Build trust with truth and keep it real._

We believe that feedback is a gift, and communicate with candor and transparency at all levels, especially with our customers. Being honest is far more kind than being 'nice' at the expense of trust. If you don't understand something, say so. If you make a mistake, own it. See something a colleague could do better? Tell them. We are all learning all the time. 


## Collaboration
_We succeed and fail together._

We love the problems we solve and the people we work with. We care about our communities, customers and each other and look for the "Win-Win-Win" whenever possible. Remember that a candle loses nothing of its flame when lighting another candle, and that your actions, both during and after your MAXX experience, reflect on us all. Be a good teammate! :)

